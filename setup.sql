CREATE DATABASE IF NOT EXISTS `teste_gesuas`;

USE `teste_gesuas`;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `nis` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
