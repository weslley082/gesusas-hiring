# Gesuas TESTE

Repositório voltado para o desenvolvimento do desafio contido no processo seletivo da GESUAS

## Instalação e Execução do código

Para executar o projeto, precisa primeiro ter o docker e o docker-compose instalado nada máquina.

Na raíz do projeto executar:

```sh
docker-compose build
```

E depois:
```sh
docker-compose up
```

Isso fará com que suba um servidor de banco de dados MySql e um apache para poder executar a aplicação

Para executar os testes, primeiro instale o composer em sua máquina e depois execute o seguinte comando na raiz do projeto:

```sh
composer install
```
E depois :
```sh
./vendor/bin/phpunit tests/
```
