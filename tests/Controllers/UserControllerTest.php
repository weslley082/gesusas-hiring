<?php

use PHPUnit\Framework\TestCase;


final class UserControllerTest extends TestCase
{
    /**@test */
    public function testWhenAddUserThenReturnUser() {
        $mockRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRepository->expects($this->once())
            ->method('addUser')
            ->with($this->equalTo("SomeUser"))
            ->willReturn(new User("SomeUser", 12345678901));

        $userController = new UserController($mockRepository);
        $user = $userController->addUser("SomeUser");
        $this->assertEquals("SomeUser", $user->getName());
        $this->assertEquals("12345678901", $user->getNis());
    }
    /**@test */
    public function testGivenExistentUserThenReturnUserWithNisAndName(){
        $mockRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRepository->expects($this->once())
            ->method('getUser')
            ->with($this->equalTo("SomeUser"))
            ->willReturn(new User("SomeUser", "12345678901"));

        $userController = new UserController($mockRepository);
        $user = $userController->addUser("SomeUser");
        $this->assertEquals("SomeUser", $user->getName());
        $this->assertEquals(12345678901, $user->getNis());
    }
    /**@test */
    public function testGivenInexistentUserThenReturnNull(){
        $mockRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRepository->expects($this->once())
            ->method('getUser')
            ->with($this->equalTo("SomeUser"))
            ->willReturn(new User("SomeUser", 12345678901));

        $userController = new UserController($mockRepository);
        $result = $userController->addUser("SomeUser");
        $this->assertEquals(null, $result);
    }

}
