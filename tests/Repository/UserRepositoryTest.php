<?php

use PHPUnit\Framework\TestCase;

class UserRepositoryTest extends TestCase
{
    /**@test */
    public function testWhenAddUserThenReturnUser()
    {
        $dbMock = $this->createMock(PDO::class);
        $dbMock->expects($this->once())
            ->method('prepare')
            ->willReturn($this->createMock(PDOStatement::class));
        $dbMock->expects($this->once())
            ->method('execute')
            ->willReturn(true);

        $userRepository = new UserRepository($dbMock);

        $name = 'SomeUser';
        $user = $userRepository->addUser($name);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($name, $user->getName());
        $this->assertNotEmpty($user->getNis());
    }
    /**@test */
    public function testGivenAExistentUserWhenCallGetUserThenReturnAUserWithNameAndNisNumber()
    {
        $dbMock = $this->createMock(PDO::class);
        $statementMock = $this->createMock(PDOStatement::class);

        $name = 'SomeUser';
        $expectedResult = [
            'name' => $name,
            'nis' => '12345678901'
        ];

        $statementMock->expects($this->once())
            ->method('execute')
            ->with(['name' => $name]);
        $statementMock->expects($this->once())
            ->method('fetch')
            ->willReturn($expectedResult);

        $dbMock->expects($this->once())
            ->method('prepare')
            ->with("SELECT * FROM users WHERE name = :name")
            ->willReturn($statementMock);

        $userRepository = new UserRepository($dbMock);

        $user = $userRepository->getUser($name);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($expectedResult['name'], $user->getName());
        $this->assertEquals($expectedResult['nis'], $user->getNis());
    }
    /**@test */
    public function testGivenAIxistentUserWhenCallGetUserThenReturnNull()
    {
        $dbMock = $this->createMock(PDO::class);
        $statementMock = $this->createMock(PDOStatement::class);

        $name = 'InexistentUser';
        $expectedResult = null;

        $statementMock->expects($this->once())
            ->method('execute')
            ->with(['name' => $name]);
        $statementMock->expects($this->once())
            ->method('fetch')
            ->willReturn($expectedResult);

        $dbMock->expects($this->once())
            ->method('prepare')
            ->with("SELECT * FROM users WHERE name = :name")
            ->willReturn($statementMock);

        $userRepository = new UserRepository($dbMock);

        $user = $userRepository->getUser($name);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($expectedResult['name'], $user->getName());
        $this->assertEquals($expectedResult['nis'], $user->getNis());
    }
}
