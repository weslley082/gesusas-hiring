<?php

require_once 'Models/User.php';

class UserRepository {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function addUser($name) {
        $nis = rand ( 10000000000 , 99999999999 );
        $user = new User($name, $nis);
        $sql = ("insert into users (name, nis) values (:name, :nis);");
        $statement = $this->db->prepare($sql);
        $statement->execute([
            'name' => $user->getName(),
            'nis' => $user->getNis()
        ]);
        return $user;
    }

    public function getUser($nis) {
        $sql = "SELECT * FROM users WHERE nis = :nis";
        $statement = $this->db->prepare($sql);
        $statement->execute([
            'nis' => $nis
        ]);

        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return ($result) ? new User($result['name'], $result['nis']) : null;
    }
}
