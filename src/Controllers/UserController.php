<?php

require_once 'Repository/UserRepository.php';

class UserController {
    private $userRepository;

    public function __construct($dbConnection) {
        $this->userRepository = new UserRepository($dbConnection);
    }

    public function addUser($name) {
        if($name == "") {
            return null;
        }
        return $this->userRepository->addUser($name);
    }

    public function getUser($nis) {
        $user = $this->userRepository->getUser($nis);
        return $user ? $user : null ;
    }
}
