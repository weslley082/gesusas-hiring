<?php
session_start();

include 'Controllers/UserController.php';
include 'Controllers/ViewController.php';

$dbConnection = new PDO("mysql:host=db;port=3306;dbname=teste_gesuas", "teste", "teste2");
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$route = str_replace('', "", $_SERVER['REQUEST_URI']);
$route = explode('/', $route);

switch ($route[1] ?? '') {
    case '':
        unset($_SESSION['user']);
        ViewController::showRegister();
        break;

    case 'search':
        unset($_SESSION['user']);
        ViewController::showSearch();
        break;
    case 'add_user':
        $userController = new UserController($dbConnection);
        if(empty($_POST['name'])) {
            $_SESSION['message'] = "Você precisa informar o nome do cidadão!";
            ViewController::showSearch();
            break;
        }
        $user = $userController->addUser($_POST['name']);
        $_SESSION['user'] = $user;
        ViewController::showRegister();
        break;
    case 'get_user':
        $userController = new UserController($dbConnection);
        if(empty($_POST['nis_number'])) {
            $_SESSION['message'] = "Número do NIS não informado";
            ViewController::showSearch();
            break;
        }
        $user = $userController->getUser($_POST['nis_number']);
        if($user == null) {
            $_SESSION['message'] = "Cidadão não encontrado";
            ViewController::showSearch();
            break;
        }
        $_SESSION['user'] = $user;
        ViewController::showSearch();
        break;
    default:
        include 'Views/404.php';
        break;

}
