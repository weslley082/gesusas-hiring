<?php
    require_once('includes/__header.php');

?>
<div class="col py-3">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h3>Cadastrar Cidadão</h3>
                    <form action="add_user" method="post">
                        <input type="text" maxlength="45" name="name" id="name" class="form-control" placeholder="Digite o nome de um cidadão">
                        <input type="submit" class="btn btn-primary" style="margin: top 4px;" value="Cadastrar">
                    </form>
                    <?php
                        if (!empty($_SESSION['user'])){
                            echo "<h3>Cidadão cadastrado</h3>";
                            echo "<p>Nome: ".$_SESSION['user']->getName()."</p>";
                            echo "<p>NIS: ".$_SESSION['user']->getNis()."</p>";
                        }
                        if (!empty($_SESSION['message'])){
                            echo $_SESSION['message'];
                            unset($_SESSION['message']);
                        }
                    ?>
            </div>
        </div>
    </div>
</div>

<?php
require_once('includes/__footer.php');
?>
