<?php
    require_once('includes/__header.php');
?>
<div class="col py-3">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h3>Buscar cidadão</h3>
                <form action="get_user" method="post">
                    <input type="number"  maxlength="11" name="nis_number" id="nis_number" class="form-control" placeholder="Digite o numero NIS de um cidadão">
                    <input type="submit" class="btn btn-primary" style="margin: top 4px;" value="Buscar">
                </form>
                <?php
                    if (!empty($_SESSION['user'])){
                        echo "<h3>Cidadão encontrado</h3>";
                        echo "<p>Nome: ".$_SESSION['user']->getName()."</p>";
                        echo "<p>NIS: ".$_SESSION['user']->getNis()."</p>";
                        unset($_SESSION['user']);
                    }
                    if (!empty($_SESSION['message'])){
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                    }

                ?>
            </div>
        </div>
    </div>
</div>

<?php
require_once('includes/__footer.php');
?>
