<?php

class User {
    private $name;
    private $nis;

    public function __construct($name, $nis) {
        $this->name = $name;
        $this->nis = $nis;
    }

    public function getName() {
        return $this->name;
    }

    public function getNis() {
        return $this->nis;
    }
}
